FROM freesurfer/freesurfer:7.2.0 AS base

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

LABEL maintainer="support@flywheel.io"

# This is here to make gear code for Freesurfer to pass tests.
ENV FREESURFER_HOME="/usr/local/freesurfer"
ENV SUBJECTS_DIR="/usr/local/freesurfer/subjects"

# Backup the existing CentOS-Base.repo file
RUN mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup

# Download a new CentOS-Base.repo file from a mirror
RUN curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

# Clean the yum cache and update
RUN yum clean all && yum makecache && yum -y update

# hadolint ignore=DL4006, DL3033
RUN (curl -sL https://rpm.nodesource.com/setup_12.x | bash -) \
  && yum clean all -y \
  && yum update -y \
  && yum install -y zip unzip nodejs tree libXt libXext ncurses-compat-libs \
  && yum clean all -y \
  && npm install npm --global

# hadolint ignore=SC1091
RUN source $FREESURFER_HOME/SetUpFreeSurfer.sh

# Extra segmentations require matlab compiled runtime
RUN fs_install_mcr R2014b

# Fix known race condition bug introduced in 7.1.1
# https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/msg68263.html
# RUN sed -i.bak '4217 s/^/#/' $FREESURFER_HOME/bin/recon-all
# The above line is already in patched recon-all along with 2nd -parallel fix
# https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/msg68878.html
RUN mv $FREESURFER_HOME/bin/recon-all $FREESURFER_HOME/bin/recon-all.bak
COPY patch/recon-all $FREESURFER_HOME/bin/recon-all

# Save environment so it can be passed in when running recon-all.
ENV PYTHONUNBUFFERED=1
RUN python -c 'import os, json; f = open("/tmp/gear_environ.json", "w"); json.dump(dict(os.environ), f)'

# Install a version of python to run Flywheel code and keep it separate from the
# python that Freesurfer uses.  Saving the environment above makes sure it is not
# changed in the Flyfwheel environment.

# Set CPATH for packages relying on compiled libs (e.g. indexed_gzip)
ENV PATH="/root/miniconda3/bin:$PATH" \
    CPATH="/root/miniconda3/include/:$CPATH" \
    LANG="C.UTF-8" \
    PYTHONNOUSERSITE=1

# hadolint ignore=DL4001, DL3047
RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-py38_4.8.3-Linux-x86_64.sh \
    && mkdir /root/.conda \
    && bash Miniconda3-py38_4.8.3-Linux-x86_64.sh -b \
    && rm -f Miniconda3-py38_4.8.3-Linux-x86_64.sh

# Installing precomputed python packages
RUN conda config --set auto_update_conda false && \
    conda install -y python=3.8.5 && \
    chmod -R a+rX /root/miniconda3; sync && \
    chmod +x /root/miniconda3/bin/*; sync && \
    conda build purge-all; sync && \
    conda clean -tipsy && sync

COPY requirements.txt /tmp
RUN pip install --no-cache-dir -r /tmp/requirements.txt && \
    rm -rf /root/.cache/pip

# Make directory for flywheel spec (v0)
ENV FLYWHEEL=/flywheel/v0
WORKDIR ${FLYWHEEL}

# Copy executable/manifest to Gear
COPY manifest.json ${FLYWHEEL}/manifest.json
COPY utils ${FLYWHEEL}/utils
COPY run.py ${FLYWHEEL}/run.py
COPY fw_gear_freesurfer_recon_all ${FLYWHEEL}/fw_gear_freesurfer_recon_all

# Configure entrypoint
RUN chmod a+x ${FLYWHEEL}/run.py
ENTRYPOINT ["/flywheel/v0/run.py"]