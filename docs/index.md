# Freesurfer Recon-All Gear Documentation

Welcome to the documentation for the Freesurfer Recon-All gear. This gear is designed
to run Freesurfer's `recon-all` pipeline, as well as a number of Freesurfer-provided
post-processing scripts.

In this documentation, you will find detailed information on how to use the Freesurfer
Recon-All gear, including usage examples, and advanced features.

Let's get started!

[Overview](./overview.md) \
[Usage and Workflow](./usages.md) \
[Gear Details](./details.md) \
[FAQs](./faqs.md) \
[Release Notes](./release_notes.md)
