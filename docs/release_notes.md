# Release Notes

## 2.0.2_7.2.0

__Enhancements__:

__Maintenance__:

__Fixes__:

* Change default value of `parallel` config option
from `True` to `False` to prevent race condition

## 2.0.1_7.2.0

__Enhancements__:

__Maintenance__:

* Updates for GitLab QA/CI and new GitLab gear exchange
* Update Dockerfile to ensure it builds

__Fixes__:

* Fix issue related to running gear using `podman` so that
extra config options do not end up in `freesurfer` command

## 2.0.0_7.2.0

__Enhancements__:

* Remove metadata writing of Freesurfer outputs to avoid breaking site indexing
* Expose hypothalamic subunits output csv file as queryable result file
* Improved documentation
* Step-by-step walk-through for building Data View querying Freesurfer output csv
files using Flywheel's SDK ViewBuilder

__Maintenance__:

* Updates for GitLab QA/CI and new GitLab gear exchange
* Documentation converted to mkdocs

__Fixes__:

* New text to csv file conversion to ensure all output csv files are in row format

## 1.2.3_7.2.0

__Enhancements__:

* Improved unit test coverage

__Maintenance__:

* Port gear from GitHub to GitLab using new skeleton template

__Fixes__:

* Fix variable names in config.json and manifest.json so that Freesurfer license
is correctly installed
* Fix gear toolkit context call so that metadata is written out correctly

## 1.2.2._7.2.0

Deprecated during final testing.

## 1.2.1_7.2.0

__Enhancements__:

__Maintenance__:

* Update version number

__Fixes__:

## 1.1.2_7.2.0

__Enhancements__:

__Maintenance__:

* Changed "master" branch to "hpc" (still in development)
* Created "main" branch to be new default branch

__Fixes__:

* re-patch recon-all to avoid "Cannot find rh.white.H" race condition

## 1.1.1_7.2.0

__Enhancements__:

__Maintenance__:

* added fs version to release

__Fixes__:

## 1.1.1

__Enhancements__:

* Upgrade Freesurfer version from 7.1 to 7.2
* Add option to run hypothalmic subunit segmentation
(<https://surfer.nmr.mgh.harvard.edu/fswiki/HypothalamicSubunits>)
* Add option to run post-processing only (if recon-all has already been run,
the output .zip file can be passed in and any selected post-processing steps
will be carried out)

__Maintenance__:

__Fixes__:

## 1.1.0_7.1.1

__Enhancements__:

* Add option to perform segmentation of thalamic nuclei after recon-all
* Add ability to use the Expert Options File
* Add optional call to gtmseg

__Maintenance__:

__Fixes__:

* Fix -parallel crash at the cortical ribbon mask stage
* Fix "Cannot find rh.white.H" when used with -parallel option.

## 1.0.0_7.1.1

__Enhancements__:

* Volumes and areas from spreadsheets are now attached as metadata
("Custom Information"
or "info") on the analysis container.

__Maintenance__:

* Now using latest Flywheel SDK version 14.5.0.

__Fixes__:

* Retry on failure: using -parallel fails randomly and re-running recon-all
should complete successfully.
