# Usage and Workflow

The `freesurfer-recon-all` gear is designed run Freesurfer's `recon-all` algorithm
and Freesurfer-provided postprocessing algorithms on a single session of data.  This
section will cover a number of common use cases of the gear.

## Single T1w image

In this case, we only have a single high-resolution T1w anatomical image
to be used for brain segmentation.

Navigate to the session containing said T1w image. If no
NIfTI image is available yet, run the `dcm2niix`
(utility) gear to convert the DICOM image to NIfTI.  After that,
run the `freesurfer-recon-all` (analysis) gear.  Click on the "anatomical"
input box to select the T1w anatomical NIfTI image (solid red arrow),
select the desired options, and run the gear.  

![alt text](assets/images/freesurfer-recon-all-docs.T1w.png)

Ensure that a Freesurfer license is included using one of three options described
[here](https://docs.flywheel.io/user/compute/gears/freesurfer/user_including_a_freesurfer_license_file_to_run_a_freesurfer_or_fmriprep_gear/).

Please note that the job takes typically between 4 and 6 hrs to run
(longer if the image contains anomalies and/or artifacts).

## Multipe T1w images

In this case, we have multiple, high-resolution T1w anatomical images
that can be used for brain segmentation.

Navigate to the session containing the multiple T1w images.
If no NIfTI images are available yet, run the `dcm2niix`
(utility) gear to convert the DICOM images to NIfTI.  After that, run the
`freesurfer-recon-all` (analysis) gear.  Click on the "anatomical"
input box to select the first T1w anatomical NIfTI image (solid red arrow), then
add the additional T1w anatomical images in "t1w_anatomical_2...5"
(dotted red arrows), select the desired options, and run the gear.

![alt text](assets/images/freesurfer-recon-all-docs.multiple-T1ws.png)

Ensure that a Freesurfer license is included using one of three options described
[here](https://docs.flywheel.io/user/compute/gears/freesurfer/user_including_a_freesurfer_license_file_to_run_a_freesurfer_or_fmriprep_gear/).

Please note that the job typically takes between 4 and 6 hrs to run
(longer if any of the images contain anomalies and/or artifacts).

## T1w and T2w images

In this case, we have both T1w, high-resolution anatomical image(s) and
a high-resolution T2w anatomical image.

Navigate to the session containing the multiple T1w images.
If no NIfTI images are available yet, run the `dcm2niix`
(utility) gear to convert the DICOM images to NIfTI.  After that, run the
`freesurfer-recon-all` (analysis) gear.  Click on the "anatomical"
input box to select the first T1w anatomical NIfTI image (solid red arrow),
then add any additional T1w anatomical images in "t1w_anatomical_2...5"
(dotted red arrows), add the T2w image to "t2_anatomical" (dashed red arrow),
select the desired options, and run the gear.  

![alt text](assets/images/freesurfer-recon-all-docs.T2w.png)

Since we are adding a T2w anatomical image, be sure to update the
`reconall_options` config option to add either `-T2pial` or `-FLAIRpial`
to the default `-all -qcache` string.

![alt text](assets/images/freesurfer-recon-all-docs.T2w-config.png)

Ensure that a Freesurfer license is included using one of three options described
[here](https://docs.flywheel.io/user/compute/gears/freesurfer/user_including_a_freesurfer_license_file_to_run_a_freesurfer_or_fmriprep_gear/).

Please note that the job typically takes between 4 and 6 hrs to run
(longer if any of the images contain anomalies and/or artifacts).

## Postprocessing only

In this case, we have already run the `freesurfer-recon-all` gear
with at least one `T1w` image file and have an existing
`freesurfer-recon-all_<subject_id>_<analysis_id>.zip` file.

Navigate to the session containing the `freesurfer-recon-all`
output zip file.  After that, run the `freesurfer-recon-all`
(analysis) gear.  Click on the "anatomical" input box to select the
`freesurfer-recon-all_<subject_id>_<analysis_id>.zip` file.  

![alt text](assets/images/freesurfer-recon-all-docs.zipfile.png)

Since we are only wishing to run the postprocessing script and not re-run the
full `recon-all` pipeline, check the `postprocessing-only` config option box
to set it to `True` (solid red arrow).  From the gear config options menu,
also check the boxes for the desired postprocessing scripts (e.g., dotted red arrows)
and uncheck the boxes for the undesired postprocessing scripts
(e.g., dotted gray arrows).

![alt text](assets/images/freesurfer-recon-all-docs.zip-config.png)

Even though the `recon-all` pipeline is not being run, ensure that a
Freesurfer license is included using one of three options described
[here](https://docs.flywheel.io/user/compute/gears/freesurfer/user_including_a_freesurfer_license_file_to_run_a_freesurfer_or_fmriprep_gear/).

**Note:** By default, the `freesurfer-recon-all` gear runs the following
postprocessing scripts: `brainstem_structures`, `convert_stats`, `convert_surfaces`,
`convert_volumes`, `hippocampal_subfields`, and `register_surfaces`.  Optional
scripts that can be added at this step (or during a full run of the
`freesurfer-recon-all` gear) are: `gtmseg`, `hypothalamic_subunits`, and
`thalamic_nuclei`.  See [Gear Details](./details.md) for descriptions
of these postprocessing scripts.

Please note that the job may take an hour or so to run depending
on the selected postprocessing scripts.

## Create dataview from output csv files

The following will walk you through the steps need to:

- Create a Flywheel Client instance for querying project
- Set the project ID to query
- Build a Flywheel ViewBuilder instance to query specific columns from
an output csv file
- Perform some basic data cleaning
- Create some simple plots using matplotlib and seaborn
- Save out the resulting DataView as a csv file

This usage example assumes that you have the Flywheel SDK installed
and an API key.  Installation instructions for Flywheel SDK can be
found
[here](https://flywheel-io.gitlab.io/product/backend/sdk/tags/17.8.0/python/getting_started.html#installation).

`# EDIT` markers are included to signpost lines where users
should feel free to update with their specific projects, csv files,
csv file columns, and output filename.

### 1. Import python modules and packages needed to run the example code

```python
# Import the necessary modules to work with the Flywheel SDK ViewBuilder,
# pandas, matplotlib, and seaborn.

import matplotlib.pyplot as plt
import os
import pandas as pd
import seaborn as sns

import flywheel
```

### 2. Set up a Flywheel Client instance

The below line assumes that you have `FW_API_KEY` (e.g.,
hostname.flywheel.io:asD45780-ajio6U1Vw) as an environment
variable. Feel free to edit the environment variable name as
needed to match your setup.

```python
# Set up Flywheel Client instance
fw = flywheel.Client(os.environ["FW_API_KEY"])
```

### 3. Check that the Flywheel Client instance is working correctly. (optional)

If you are setting up a Flywheel Client instance for the first
time, uncomment the below two lines to verify that the above
step is working correctly.  These two lines will print out the first
and last name of the user associated with the API key used above.  This
is simply a sanity check to make sure everything is working before
trying to query a project.

```python
# Sanity check that Client instance is working
self = fw.get_current_user()
print('I am %s %s' % (self.firstname, self.lastname))
```

### 4. Define the Group and Project labels

Edit the below two variables to set the desired `GROUP`
and `PROJECT` to query.

```python
# Group ID associated with the project
GROUP = "group1"  # EDIT

# Project label with freesurfer-recon-all data
PROJECT = "project1"   # EDIT
```

### 5. Set up the project to query

Use the above `GROUP` and `PROJECT` variables to set up the project to
query.  Print out confirmation that the correct project and group is set.

```python
# Set up group and project to query
project = fw.lookup(f"{GROUP}/{PROJECT}")
print(f"Working with project {project.label} (group {GROUP})")
```

### 6. Set the CSV file and columns to query

For `CSV_FILENAME`, specify the csv filename to query.  In this
case, remove the `subject_id` prefix from the start of the filename.  
The list of avaiable csv files to query will depend on the options
selected when running the `freesurfer-recon-all` gear.  Users can
consult the list of `OUTPUTS` in the Flywheel UI under
`Group->Project->Session->Analyses->Results`.

For `CSV_COLUMNS`, specify a list of one or more columns from the
`CSV_FILENAME` to pull.  The column names specified must match
exactly the column names in the csv file.

```python
# CSV file from which to extract data
CSV_FILENAME = 'aseg_stats_vol_mm3.csv'  # EDIT

# CSV file columns to be used
CSV_COLUMNS = [                          # EDIT
                'Left-Hippocampus',
                'Right-Hippocampus',                   
              ]
```

### 7. Specify the gear version

Specify the version of the `freesurfer-recon-all` gear run on the
project (e.g., 1.2.1_7.2.0).  If unsure, the gear version can be found
in the UI under `Group->Project->Session->Analyses->Gear Information`.

```python
# freesurfer-recon-all gear version
GEAR_VERSION  = "1.2.2_7.2.0_rc1" # EDIT
```

### 8. Specify and build the DataView

Unless you know what you are doing, there should be no need to edit
anything in the below code block if querying any of the standard csv
files created by the `freesurfer-recon-all` gear.

```python
# CREATE DATA VIEW

# Define fixed columns related to the project, subject, session, and analysis
# to include in the dataview.  These will be helpful for filtering and sorting.
columns = [ 'project.label',
            'project.id',
            'subject.label',
            'subject.id',
            'session.label',
            'session.id',
            'analysis.label',
            'analysis.id',
          ]

# Specify the dataview
builder = flywheel.ViewBuilder(label = 'Data View for freesurfer-recon-all',
                               columns = columns,
                               container ='session',
                               analysis_gear_name = 'freesurfer-recon-all',
                               filename = f'.*_{CSV_FILENAME}',
                               match ='newest',  # 'newest'|'all'
                               process_files = True,
                               include_ids = False,
                               include_labels = False,
                               sort = False,
                               )
for c in CSV_COLUMNS: builder.file_column(c)

# Create the dataview specification

# 1. Filter by gear_version and the string in the label, everything 
# that starts with freesurfer-recon-all will be selected (* works as a wildcard)
builder.analysis_filter(gear_version=GEAR_VERSION,label='freesurfer-recon-all*')

# 2. Filter by the csv file name, allowing string to be interpreted as a regex
# Since all files start with <subject_id>_<filename>.csv, allow 
<subject_id> to be a wildcard
builder.file_filter(f'.*_{CSV_FILENAME}', regex=True)


sdk_dataview = builder.build()
sdk_dataview.parent = project.id
```

### 9. Query the project and create the dataview

Again, no edits in the below code block are needed.  This code will
query the `CSV_FILENAME` in the specified `GROUP/PROJECT` and return
a Dataview of the `columns` and `CSV_COLUMNS` for all subjects/
sessions.  Depending on the size of the project, this step may
take a few minutes.

```python
# Query project and build the dataview
dv = fw.read_view_dataframe(sdk_dataview, project.id)
```

### 10. Examine and explore the dataview

At this point, `dv` should contain the data for each subject and session
from `CSV_FILENAME` and `CSV_COLUMNS` as a pandas dataframe.  All the
normal pandas functionality should work.

For example, the below code block `dv.head()` will print out
the first five lines to aid in checking for errors.

```python
# Preview the first five lines of the dataview
dv.head()
```

### 11. Remove any subjects/sessions with `NaN` in `CSV_COLUMNS`

As an initial step to clean up the dataview, the below code block will
check the `CSV_COLUMNS` defined above for the data pulled from
`CSV_FILENAME` and remove any rows with `NaN` values.

```python
# Remove any rows that have NaN values in the CSV_COLUMNS
dv.dropna(subset=CSV_COLUMNS, inplace=True)
```

### 12. Sample scatter plots using `seaborn` and `matplotlib`

The worked-through example in this notebook pulls the volume of the Left Hippocampus
and Right Hippocampus from `aseg_stats_vol_mm3.csv`.  Below are examples on how to
scatter plot the volumes of the Right and Left Hippocampus
using `seaborn` and then `matplotlib`.

```python
# Scatter plot using seaborn
sns.scatterplot(data=dv, x=CSV_COLUMNS[0], y=CSV_COLUMNS[1])   
```

```python
# Scatter plot using matplotlib
plt.scatter(dv[CSV_COLUMNS[0]], dv[CSV_COLUMNS[1]])
plt.xlabel(CSV_COLUMNS[0])
plt.ylabel(CSV_COLUMNS[1])
plt.show()
```

### 13. Save out the dataview as a csv file

To faciliate offline use of the dataview, the below code block contains the
necessary line to save out the dataview as a csv file.  Specify the
desired `OUTPUT_FILENAME` below.

```python
# Output filename for the dataview csv file
OUTPUT_FILENAME = 'output_dataview.csv'  # EDIT
```

```python
# Save the dataview to a CSV file
dv.to_csv(OUTPUT_FILENAME, index=False)  
```
