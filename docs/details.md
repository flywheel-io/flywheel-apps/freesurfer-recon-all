<!-- markdownlint-disable MD024 -->
# Gear Details

This section provides detailed information about the gear, including its inputs,
outputs, and configuration options.

## Cite

For citation information, please visit:
<https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferMethodsCitation>.

## License

BSD-3-Clause: <https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferSoftwareLicense>

## Classification

* **Species:** Human
* **Organ:** Brain
* **Therapeutic Area:** Neuro
* **Modality:** MRI
* **File Type:**  NIfTI
* **Function:** Analysis
* **Suite:** Analysis
* **Category:** Analysis
* **Gear Permission:** Read-only
* **Container Level:**
  * [X] Project
  * [X] Subject
  * [X] Session
  * [X] Acquisition
  * [X] Analysis

## Inputs

### Files

#### anatomical

* **Type**: `nifti`
* **Optional**: `false`
* **Description**: T1w anatomical NIfTI file
* **Notes**: T1w anatomical NIfTI file or previous freesurfer-recon-all
  zip archive (required for gear-postprocessing-only option)

#### t1w_anatomical_2

* **Type**: `nifti`
* **Optional**: `true`
* **Description**: T1w anatomical NIfTI file
* **Notes**: Additional T1w anatomical NIfTI file. These will be averaged together to
  provide for better motion correction.

#### t1w_anatomical_3

* **Type**: `nifti`
* **Optional**: `true`
* **Description**: T1w anatomical NIfTI file
* **Notes**: Additional T1w anatomical NIfTI file. These will be averaged together to
  provide for better motion correction.

#### t1w_anatomical_4

* **Type**: `nifti`
* **Optional**: `true`
* **Description**: T1w anatomical NIfTI file
* **Notes**: Additional T1w anatomical NIfTI file. These will be averaged together to
  provide for better motion correction.

#### t1w_anatomical_5

* **Type**: `nifti`
* **Optional**: `true`
* **Description**: T1w anatomical NIfTI file
* **Notes**: Additional T1w anatomical NIfTI file. These will be averaged together to
  provide for better motion correction.

#### t2w_anatomical

* **Type**: `nifti`
* **Optional**: `true`
* **Description**: T2 or FLAIR data to improve pial surfaces
* **Notes**: T2 or FLAIR data to improve pial surfaces. This can be NIfTI or DICOM.
  The `-T2pial` or `-FLAIRpial` flags will need to be added in the
  `reconall_options` configuration parameter.

#### freesurfer_license

* **Type**: `file`
* **Optional**: `true`
* **Description**: FreeSurfer license file
* **Notes**: A license is required for this gear to run but it does not have to
  be provided as an input file. There are
  [three ways](https://docs.flywheel.io/user/compute/gears/freesurfer/user_including_a_freesurfer_license_file_to_run_a_freesurfer_or_fmriprep_gear/)
  to provide the license to this gear.
  [Obtaining a license](https://surfer.nmr.mgh.harvard.edu/registration.html)
  is free. If you select a file here, it will be copied into the `$FSHOME` directory
  when the gear runs before launching recon-all.

#### expert

* **Type**: `file`
* **Optional**: `true`
* **Description**: User-created file containing special options to include in the
command string.
* **Notes**: A user-created file containing special options to include in the
command string.
  The file should contain as the first item the name of the command,
  and the items following it on rest of the line will be passed as the
  extra options. See
  [Freesurfer documentation](https://surfer.nmr.mgh.harvard.edu/fswiki/recon-all#ExpertOptionsFile)
  for more information and examples.

### Configuration

#### subject_id

* **Type**: `string`
* **Description**: Desired subject ID. Any spaces in the `subject_id` will be
  replaced with underscores and will be used to name the resulting FreeSurfer
  output directory. NOTE: If using a previous Gear output as input the subject
  code will be parsed from the input archive, however it should still be provided
  here for good measure.
* **Default**:

#### reconall_options

* **Type**: `string`
* **Description**: Command line options to the recon-all algorithm. By default
  we enable `-all` and `-qcache`. `-all` runs the entire pipeline and `-qcache`
  will resample data onto the average subject (called fsaverage) and smooth it
  at various FWHM (full-width/half-max) values, usually 0, 5, 10, 15, 20, and
  25mm, which can speed later processing. Note that modification of these options
  may result in failure if the options are not recognized.
* **Default**: `-all -qcache`

#### parallel

* **Type**: `boolean`
* **Description**: Command line option to run recon-all in parallel. Setting to
  `True` will instruct the binaries to use 4 processors (cores), meaning,
  4 threads will run in parallel in some operations. Adjust n_cpus for more
  (or less) than 4 cores.
* **Default**: `false`

#### n_cpus

* **Type**: `integer`
* **Description**: Command line option to set the number of processors to use in
  recon-all execution. (optional, defaults to 'core_count' within host). Overridden
  by including the flag `-openmp <num>` after `-parallel`, where `<num>` is the number
  of processors you'd like to use. Minimum of 1. If set greater than the cores
  available, that maximum will be used. If 'parallel' is False, this is disregarded.
* **Default**: `true`

#### gear-brainstem_structures

* **Type**: `boolean`
* **Description**: Generate automated segmentation of four different brainstem
  structures from the input T1 scan: medulla oblongata, pons, midbrain and superior
  cerebellar peduncle (SCP). Script uses a Bayesian segmentation algorithm that relies
  on a probabilistic atlas of the brainstem (and neighboring brain structures)
  built upon manual delineations of the structures on interest in 49 scans
  (10 for the brainstem structures, 39 for the surrounding structures). The
  delineation protocol for the brainstem was designed by Dr. Adam Boxer and his
  team at the UCSF Memory and Aging Center and is described in the paper.
  Choosing this option will write `<subject_id>_BrainstemStructures.csv` to the
  final results. See:
  <https://surfer.nmr.mgh.harvard.edu/fswiki/BrainstemSubstructures> for more info.
* **Default**: `true`

#### gear-convert_stats

* **Type**: `boolean`
* **Description**: Convert FreeSurfer stats files to CSV. Converts a subcortical
  stats file created by recon-all and/or mri_segstats (eg, aseg.stats) into a table
  in which each line is a subject and each column is a segmentation. The values are
  the volume of the segmentation in mm3 or the mean intensity over the structure. Also
  converts a cortical stats file created by `recon-all` and or `mris_anatomical_stats`
  (eg, `?h.aparc.stats`) into a table in which each line is a subject and each column
  is a parcellation. By default, the values are the area of the parcellation in mm2.
* **Default**: `true`

#### gear-convert_surfaces  

* **Type**: `boolean`
* **Description**: Convert selected FreeSurfer surface files to OBJ format.
* **Default**: `true`

#### gear-convert_volumes

* **Type**: `boolean`
* **Description**: Convert selected FreeSurfer volume files (mgz) to NIfTI format.
* **Default**: `true`

#### gear-dry-run

* **Type**: `boolean`
* **Description**: Do everything except actually executing recon-all.
* **Default**: `false`

#### gear-FREESURFER_LICENSE

* **Type**: `string`
* **Description**: Text from license file generated during FreeSurfer registration.
  Entries should be space separated.
* **Default**:

#### gear-gtmseg

* **Type**: `boolean`
* **Description**: After running recon-all run `gtmseg` (geometric transfer
  matrix segmentation) on the subject.
* **Default**: `false`

#### gear-hippocampal_subfields

* **Type**: `boolean`
* **Description**: Generates an automated segmentation of the hippocampal subfields
  based on a statistical atlas built primarily upon ultra-high resolution
  (~0.1 mm isotropic) ex vivo MRI data. Choosing this option will write
  `<subject_id>_HippocampalSubfields.csv` to the final results.
  See: <https://surfer.nmr.mgh.harvard.edu/fswiki/HippocampalSubfields> for more info.
* **Default**: `true`

#### gear-hypothalamic_subunits

* **Type**: `boolean`
* **Description**: After running recon-all run segmentation of hypothalamic
  subunits (`mri_segment_hypothalamic_subunits`) on the subject.
* **Default**: `false`

#### gear-log-level

* **Type**: `string`
* **Description**: Gear Log verbosity level (`INFO`|`DEBUG`).
* **Default**: `INFO`

#### gear-postprocessing-only

* **Type**: `boolean`
* **Description**: If a previous recon-all zip file is given as input,
  do not re-run recon-all.  Instead only run the additional post-processing commands.  
  The recon-all zip file should be supplied in place of the `anatomical`.
* **Default**: `false`

#### gear-register_surfaces

* **Type**: `boolean`
* **Description**: Runs the `xhemireg` and `surfreg` scripts on subject after
  having run recon-all in order to register the subject's left and inverted-right
  hemispheres to the `fsaverage_sym` subject. (The `fsaverage_sym` subject is a version
  of the `fsaverage` subject with a single the left-right symmetric pseudo-hemisphere.)
* **Default**: `true`

#### gear-thalamic_nuclei

* **Type**: `boolean`
* **Description**: Produce a parcellation of the thalamus into 25 different nuclei,
  using a probabilistic atlas built with histological data. Choosing this option
  will produce 3 files in the subject's mri directory:
  `ThalamicNuclei.v12.T1.volumes.txt`,
  `ThalamicNuclei.v12.T1.mgz`, and `ThalamicNuclei.v12.T1.FSvoxelSpace.mgz`, and
  2 files in the stats directory: `thalamic-nuclei.lh.v12.T1.stats` and
  `thalamic-nuclei.rh.v12.T1.stats`. See:
  <https://surfer.nmr.mgh.harvard.edu/fswiki/ThalamicNuclei> for more info.
* **Default**: `false`

## Outputs

### Files

* `freesurfer-recon-all_<subject_id>_<analysis_id>.zip`
  * **Type**: zip
  * **Optional**: false
  * **Classification**: zip
  * **Description**: All files that are the results of `recon-all` in the Freesurfer
  subject directory are compressed into a single zip archive.
  * **Notes**: See the "Introduction to Freesurfer Output" tutorial
  [here](https://surfer.nmr.mgh.harvard.edu/fswiki/Tutorials) for details
  on the contents of a standard Freesurfer subject directory.

## Pre-requisites

This gear has no prerequisites beyond needing a `T1w` NIfTI file and
a Freesurfer `license.txt` file.

### Prerequisite Gear Runs

A list of gears, in the order they need to be run:

1. **dcm2niix**
    * Level: Acquisition

### Prerequisite Files

None beyond those specified above as inputs.

## Workflow

The gear runs Freesurfer `recon-all` using the specified inputs and configurations.  
See <https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferWiki> for a general overview
and <https://surfer.nmr.mgh.harvard.edu/fswiki/ReconAllDevTable> for the specific details
of each step.

Here is a representation of the
[`recon-all` workflow](https://surfer.nmr.mgh.harvard.edu/fswiki/ReconAllBlockDiagram)

![Block_diag1](https://surfer.nmr.mgh.harvard.edu/fswiki/ReconAllBlockDiagram?action=AttachFile&do=get&target=Block_diag1.jpg)
![Block_diag2](https://surfer.nmr.mgh.harvard.edu/fswiki/ReconAllBlockDiagram?action=AttachFile&do=get&target=Block_diag2.jpg)
