# Frequently Asked Questions

* Can I add custom options to the `recon-all` command?

    Yes!  There are two ways to add options to the `recon-all`
    command.  First, you can edit the gear config `reconall_options` to
    add options or edit the default options.  See
    [here](details.md#reconall_options).  For more advanced options,
    you can provide an `expert.opts` file as an input to the gear.  See
    [here](details.md#expert) for more information on the `expert.opts`
    file and a link to the Freesurfer documentation on this file.

* How can I fix this error? `FileNotFoundError: Could not find FreeSurfer license anywhere.`

    This error message means that a Freesurfer `license.txt` file
    was not found.  Please check that you have correctly supplied
    a `license.txt` file to the gear using one of the three methods
    described
    [here](https://docs.flywheel.io/user/compute/gears/freesurfer/user_including_a_freesurfer_license_file_to_run_a_freesurfer_or_fmriprep_gear/).
    If supplying the license information via the gear config option
    (described [here](details.md#gear-FREESURFER_LICENSE)),
    make sure that you have entered all of the lines in the `license.txt`
    file separated by a single space.

* How long does it take to run the gear?

    If running the full `recon-all` pipeline, this gear usually takes 4-6
    hours to complete, longer if the input image or images have anomolies or
    artifacts.  The `recon-all` pipeline will also take longer to run
    for high-field, high-resolution input image(s).  Running just the
    postprocessing scripts may take upwards of 1 hour (usually less)
    depending on the number of scripts and which scripts are run.
