<!-- markdownlint-disable MD024 -->
# Overview

## Description

This gear runs both Freesurfer `recon-all` and a number of Freesurfer-provided
postprocessing scripts.  Image results are written out in both `nifti` and `obj`
formats (the latter to facilitate viewing within the Flywheel interface).  Tabular results
are written out as `csv` files.  Finally, the full Freesurfer
output directory is written out as a `zip` file.  This gear does not
write out any metadata.

## Input

### Full recon-all pipeline

#### Required inputs

At minimum, the gear requires a single `T1w` nifti file as input. The
following file type is currently supported:

* NIfTI

#### Optional inputs

The gear can take up to four additional `T1w` nifti image
files and one `T2w` nifti image file as input.  The following file type
is currently supported:

* NIfTI

The gear can also take a user-created text file of special options to
include in the Freesurfer command string.  See
[Freesurfer documentation](https://surfer.nmr.mgh.harvard.edu/fswiki/recon-all#ExpertOptionsFile)
for more information and examples.  The following file type is currently
supported:

* `expert.opts` (text file)

### Postprocessing scripts only

#### Required inputs

The postprocessing functions can be separately run on an existing Freesurfer
output `zip` file.  In this case, the gear takes the Freesurfer output `zip`
file instead of the required `T1w` file.  The following file type is supported:

* `freesurfer-recon-all_<subject_id>_<analysis_id>.zip`

#### Optional inputs

There are no optional inputs.

### Freesurfer license requirement

The gear requires a Freesurfer `license.txt`
[file](https://surfer.nmr.mgh.harvard.edu/registration.html) supplied as an
input file, a config option, or as a file associated with the project.  See
the following
[Flywheel guide](https://docs.flywheel.io/user/compute/gears/freesurfer/user_including_a_freesurfer_license_file_to_run_a_freesurfer_or_fmriprep_gear/)
on how to supply a Freesurfer `license.txt` file.  

**Note:** A Freesurfer license is required when running both the
full `recon-all` pipeline and any of the postprocessing scripts.

## Output

The primary output consists of `zip` file
(`freesurfer-recon_all_<subject_id>_<analysis_id>.zip`)
containing all of the Freesurfer `recon-all` and postprocessing output.  Details
of the files contained in the `zip` file can be found
[here](https://surfer.nmr.mgh.harvard.edu/fswiki/Tutorials).

## Classification
<!-- markdownlint-disable MD032 -->
* Maintainer: <img src="https://img.shields.io/badge/Flywheel-1B68FA" alt="Flywheel">  
* Therapeutic Area: <img src="https://img.shields.io/badge/Neuro-1B68FA" alt="neuro">
* Modality: <img src="https://img.shields.io/badge/MRI-1B68FA" alt="MRI">  
* Suite: <img src="https://img.shields.io/badge/Analysis-1B68FA" alt="Analysis">  
* File Type: <img src="https://img.shields.io/badge/nifti-1B68FA" alt="nfiti">
* GPU: <img src="https://img.shields.io/badge/No-1B68FA" alt="No">
<!-- markdownlint-enable MD032 -->
