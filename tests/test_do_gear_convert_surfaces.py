"""Unit test for do_gear_convert_surfaces.py"""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import do_gear_convert_surfaces

log = logging.getLogger(__name__)

MOCK_ENVIRON = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}
MOCK_SURFACE_FILES = [
    "lh.pial",
    "rh.pial",
    "lh.white",
    "rh.white",
    "rh.inflated",
    "lh.inflated",
]


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_do_gear_convert_surfaces_dry_run_false(
    mock_exec_command,
    mock_subject_dir,  # Defined in conftest.py
    flywheel_base,  # Defined in conftest.py
    output_dir,  # Defined in conftest.py
):
    """Test do_gear_convert_surfaces."""

    mock_dry_run = False
    mock_surf_dir = mock_subject_dir / "surf"

    with patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", flywheel_base), patch(
        "fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", output_dir
    ):
        do_gear_convert_surfaces(
            mock_subject_dir,
            mock_dry_run,
            MOCK_ENVIRON,
            log,
        )

    # Expect two exec_command calls for each
    # of six surface files in mock_surface_files.
    assert mock_exec_command.call_count == len(MOCK_SURFACE_FILES) * 2

    # Build list of arguments for each expected call to exec_command
    expected_calls = []
    for sf in MOCK_SURFACE_FILES:
        expected_calls.append(
            ["mris_convert", f"{mock_surf_dir}/{sf}", f"{mock_surf_dir}/{sf}.asc"]
        )
        expected_calls.append(
            [
                f"{flywheel_base}/utils/srf2obj",
                f"{mock_surf_dir}/{sf}.asc",
                ">",
                f"{output_dir}/{sf}.obj",
            ]
        )

    # Check that each expected call of exec_command was made
    # with correct args and kwargs
    for c, cll in enumerate(expected_calls):
        mock_call = mock_exec_command.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll
        if "mris_convert" in cll:
            assert kwargs == {
                "environ": MOCK_ENVIRON,
                "dry_run": mock_dry_run,
                "cont_output": True,
            }
        elif "srf2obj" in cll:
            assert kwargs == {
                "environ": MOCK_ENVIRON,
                "shell": True,
                "dry_run": mock_dry_run,
                "cont_output": True,
            }


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_do_gear_convert_surfaces_dry_run_true(
    mock_exec_command,
    mock_subject_dir,  # Defined in conftest.py
    flywheel_base,  # Defined in conftest.py
    output_dir,  # Defined in conftest.py
):
    """Test do_gear_convert_surfaces."""

    mock_dry_run = True
    mock_surf_dir = mock_subject_dir / "surf"

    with patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", flywheel_base), patch(
        "fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", output_dir
    ):
        do_gear_convert_surfaces(
            mock_subject_dir,
            mock_dry_run,
            MOCK_ENVIRON,
            log,
        )

    # Expect two exec_command calls for each
    # of six surface files in mock_surface_files.
    assert mock_exec_command.call_count == len(MOCK_SURFACE_FILES) * 2

    # Build list of arguments for each expected call to exec_command
    expected_calls = []
    for sf in MOCK_SURFACE_FILES:
        expected_calls.append(
            ["mris_convert", f"{mock_surf_dir}/{sf}", f"{mock_surf_dir}/{sf}.asc"]
        )
        expected_calls.append(
            [
                f"{flywheel_base}/utils/srf2obj",
                f"{mock_surf_dir}/{sf}.asc",
                ">",
                f"{output_dir}/{sf}.obj",
            ]
        )

    # Check that each expected call of exec_command was made
    # with correct args and kwargs
    for c, cll in enumerate(expected_calls):
        mock_call = mock_exec_command.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll
        if "mris_convert" in cll:
            assert kwargs == {
                "environ": MOCK_ENVIRON,
                "dry_run": mock_dry_run,
                "cont_output": True,
            }
        elif "srf2obj" in cll:
            assert kwargs == {
                "environ": MOCK_ENVIRON,
                "shell": True,
                "dry_run": mock_dry_run,
                "cont_output": True,
            }
