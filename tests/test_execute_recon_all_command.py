"""Unit test for execute_recon_all_command."""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import execute_recon_all_command

log = logging.getLogger(__name__)

mock_command = (
    "recon-all -i /input/sub-01/anat/sub-01_T1w.nii.gz -s sub-01 -all -qcache "
    " -parallel -openmp 4 -sd /output -expert /flywheel/v0/config/recon-all.opts"
)

mock_environ = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}
mock_metadata = {}


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_execute_recon_all_command_successful_run(mock_exec_command, mock_subject_dir):
    """Test the call execute_recon_all_command for a successful run."""
    expected_errors = []
    expected_warnings = []
    expected_return_code = 0
    errors, warnings, return_code, metadata = execute_recon_all_command(
        mock_command,
        environ=mock_environ,
        dry_run=False,
        subject_dir=mock_subject_dir,
        log=log,
        metadata=mock_metadata,
    )

    # assertions:
    mock_exec_command.assert_called_once()
    assert errors == expected_errors
    assert warnings == expected_warnings
    assert return_code == expected_return_code


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_execute_recon_all_command_dryrun(mock_exec_command, mock_subject_dir):
    """Test the call execute_recon_all_command for a dryrun."""
    expected_errors = []
    expected_warnings = ["gear-dry-run is set: Command was NOT run."]
    expected_return_code = 0
    errors, warnings, return_code, metadata = execute_recon_all_command(
        mock_command,
        environ=mock_environ,
        dry_run=True,
        subject_dir=mock_subject_dir,
        log=log,
        metadata=mock_metadata,
    )

    # assertions:
    mock_exec_command.assert_called_once()
    assert errors == expected_errors
    assert warnings == expected_warnings
    assert return_code == expected_return_code


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
@patch("fw_gear_freesurfer_recon_all.main.remove_i_args")
def test_execute_recon_all_command_runtime_error(
    mock_exec_command, mock_remove_i_args, mock_subject_dir
):
    """Test the call execute_recon_all_command for a RuntimeError."""

    # mock the exec_command function to raises two RunTime Errors
    # to check call is executed twice
    expected_errors = [RuntimeError("Unable to execute command.")] * 2
    expected_warnings = []
    expected_return_code = 1
    with patch(
        "fw_gear_freesurfer_recon_all.main.exec_command",
        side_effect=RuntimeError("Unable to execute command."),
    ):
        errors, warnings, return_code, metadata = execute_recon_all_command(
            mock_command,
            environ=mock_environ,
            dry_run=False,
            subject_dir=mock_subject_dir,
            log=log,
            metadata=mock_metadata,
        )

        # assertions:
        assert str(errors) == str(expected_errors)  # convert to strings to compare
        assert warnings == expected_warnings
        assert return_code == expected_return_code
