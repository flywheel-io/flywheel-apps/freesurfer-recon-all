"""Module to test parser.py."""

from unittest.mock import MagicMock, call

import pytest
from flywheel_gear_toolkit.context import GearToolkitContext

from fw_gear_freesurfer_recon_all.parser import parse_config


def test_parse_config():
    """Test for parse_config.

    Make sure that the function returns two dicts: one with inputs,
    one with config options.  Inputs should be called 8 times, and
    config should be called 0 times.

    Also test that inputs calls "anatomical" once.

    """

    gear_context = MagicMock(spec=GearToolkitContext)
    parse_config(gear_context)

    # Set input_path.call_count to 8 to match maximum number of inputs
    assert gear_context.get_input_path.call_count == 8

    # Set get_input.call_count to 0, as gear_configs has no mandatory options
    assert gear_context.get_input.call_count == 0

    # Check that gear_context.get_input_path is called with argument "anatomical"
    calls = [call("anatomical")]
    gear_context.get_input_path.assert_has_calls(calls, any_order=True)


def test_parser_empty_anatomical():
    """Test for parse_config when anatomical input is empty.

    Make sure that if "anatomical" is empty, parser raises an error and exits.

    """

    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.get_input_path.return_value = None

    with pytest.raises(SystemExit):
        parse_config(gear_context)
