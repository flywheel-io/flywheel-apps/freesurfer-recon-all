"""Unit test for remove_i_args."""

import pytest

from fw_gear_freesurfer_recon_all.main import remove_i_args

commands_1 = [
    "time",
    "recon-all",
    "-i",
    "path/to/that/T1",
    "-all",
    "-whatever",
    "hey",
    "-i",
    "another/path/to/input/scan",
    "-3T",
]
expected_commands_1 = ["time", "recon-all", "-all", "-whatever", "hey", "-3T"]
commands_2 = expected_commands_2 = [
    "time",
    "recon-all",
    "-all",
    "-whatever",
    "hey",
    "-3T",
]


@pytest.mark.parametrize(
    "commands, expected_commands",
    [
        (commands_1, expected_commands_1),
        (commands_2, expected_commands_2),
    ],
)
def test_remove_i_args_works(commands, expected_commands):
    resume_command = remove_i_args(commands)
    print(resume_command)

    assert "-i" not in resume_command
    assert resume_command == expected_commands
