"""Unit test for despace."""

import pytest

from utils.fly.despace import despace

data = [
    (
        "root",
        ["a dir with spaces/afile", "a dir with spaces/afile with spaces"],
        ["root/a_dir_with_spaces/afile", "root/a_dir_with_spaces/afile_with_spaces"],
    ),
]


@pytest.mark.parametrize("root_dir, files, expected_files", data)
def test_despace_works(tmp_path, root_dir, files, expected_files):
    """Test that despace works.

    Test that despace correctly replaces spaces with
    underscores in both directory and filenames.

    """

    # Create root directory
    root_path = tmp_path / root_dir
    root_path.mkdir(parents=True, exist_ok=True)

    # Create files
    for afile in files:
        file_path = root_path / afile
        file_path.parent.mkdir(parents=True, exist_ok=True)
        file_path.touch()

    # Run despace on the root directory
    despace(root_path)

    # Check results for files
    for expected_file in expected_files:
        assert (tmp_path / expected_file).exists()
