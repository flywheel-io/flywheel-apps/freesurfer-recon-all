"""Unit test for set_core_count."""

import logging
import os

from fw_gear_freesurfer_recon_all.main import set_core_count

log = logging.getLogger(__name__)


def test_core_count_empty_works():
    """Test that set_core_count with empty config returns os.cpu_count."""

    config = dict()

    set_core_count(config, log)

    assert config["openmp"] == os.cpu_count()


def test_core_count_0_gets_max():
    """Test that set_core_count with n_cpu=0 returns os.cpu_count."""

    config = {"n_cpus": 0}

    set_core_count(config, log)

    assert config["openmp"] == os.cpu_count()


def test_core_count_toomuch_gets_max(caplog):
    """Test that set_core_count with more than max cpus returns os.cpu_count."""

    config = {"n_cpus": 1000}

    set_core_count(config, log)

    assert str(os.cpu_count()) in caplog.text

    assert any(
        str(os.cpu_count()) in message and level == logging.WARNING
        for _, level, message in caplog.record_tuples
    )

    assert config["openmp"] == os.cpu_count()
    assert "n_cpus" not in config


def test_core_count_1_gets_1():
    """Test that set_core_count works with user-specified cpus."""

    num_cpus = 1
    config = {"n_cpus": num_cpus}

    set_core_count(config, log)

    assert config["openmp"] == num_cpus
    assert "n_cpus" not in config
