"""Unit test for do_gear_convert_volumes.py"""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import do_gear_convert_volumes
from tests.conftest import ASSETS_ROOT

log = logging.getLogger(__name__)


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_do_gear_convert_volumes(
    mock_exec_command,
    mock_subject_dir,  # Defined in conftest.py
):
    """Test do_gear_convert_volumes for mock config."""

    mock_dry_run = False
    mock_environ = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}
    mock_config = {
        "gear-convert_volumes": False,
        "gear-convert_stats": False,
        "gear-convert_surfaces": False,
        "gear-register_surfaces": False,
        "gear-hippocampal_subfields": True,
        "gear-brainstem_structures": True,
        "gear-thalamic_nuclei": False,
        "gear-hypothalamic_subunits": False,
        "gear_gtmseg": False,
    }

    mock_mri_mgz_files = [
        "aparc+aseg.mgz",
        "aparc.a2009s+aseg.mgz",
        "brainmask.mgz",
        "lh.ribbon.mgz",
        "rh.ribbon.mgz",
        "ribbon.mgz",
        "aseg.mgz",
        "orig.mgz",
        "T1.mgz",
        "lh.hippoAmygLabels-T1.v21.FSvoxelSpace.mgz",  # Hippocampal subfields
        "rh.hippoAmygLabels-T1.v21.FSvoxelSpace.mgz",  # Hippocampal subfields
        "brainstemSsLabels.v12.FSvoxelSpace.mgz",  # Brainstem structures
    ]

    mock_mri_dir = mock_subject_dir / "mri"

    with patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", ASSETS_ROOT):
        do_gear_convert_volumes(
            mock_config,
            mock_mri_dir,
            mock_dry_run,
            mock_environ,
            log,
        )

    # Expect one call for each file in mock_mri_mgz_files
    # Number of files depends on config settings
    assert mock_exec_command.call_count == len(mock_mri_mgz_files)

    # Build list of arguments for each expected call to exec_command
    expected_calls = []
    for file in mock_mri_mgz_files:
        expected_calls.append(
            [
                "mri_convert",
                "-i",
                f"{mock_mri_dir}/{file}",
                "-o",
                f"{ASSETS_ROOT}/{file.replace('.mgz', '.nii.gz')}",
            ]
        )

    # Check that each expected call of exec_command was made
    # with correct args and kwargs
    for c, cll in enumerate(expected_calls):
        mock_call = mock_exec_command.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll
        assert kwargs == {
            "environ": mock_environ,
            "dry_run": mock_dry_run,
            "cont_output": True,
        }
