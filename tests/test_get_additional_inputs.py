"""Unit tests for get_additional_inputs."""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import get_additional_inputs

log = logging.getLogger(__name__)


@patch("fw_gear_freesurfer_recon_all.main.despace")
def test_additional_t1ws(mock_despace, flywheel_base, input_dir, create_input_file):
    """Check that additional T1w images are found and added to list."""

    with patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", flywheel_base), patch(
        "fw_gear_freesurfer_recon_all.main.INPUT_DIR", input_dir
    ):
        # delete existing and create new input directories and add nifti files
        anat_file_2, anat_file_3 = (
            create_input_file(
                anat_dir_name=f"t1w_anatomical_{i}",
                anat_file_name=f"T1_run-0{i}.nii.gz",
                touch_file=True,
            )
            for i in [2, 3]
        )
        anat_file_4, anat_file_5 = (
            create_input_file(
                anat_dir_name=f"t1w_anatomical_{i}",
                anat_file_name=f"T1_run-0{i}.nii.gz",
                touch_file=False,
            )
            for i in [4, 5]
        )

        add_inputs = get_additional_inputs(log)

    # Assert T1w files are found
    assert anat_file_2 in add_inputs
    assert anat_file_3 in add_inputs
    assert anat_file_4 not in add_inputs
    assert anat_file_5 not in add_inputs

    # Assert '-i' in add_inputs (count = num T1w files)
    assert add_inputs.count("-i") == 2


@patch("fw_gear_freesurfer_recon_all.main.despace")
def test_additional_t2(mock_despace, flywheel_base, input_dir, create_input_file):
    "Check that additional T2w image is found and added to list."

    with patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", flywheel_base), patch(
        "fw_gear_freesurfer_recon_all.main.INPUT_DIR", input_dir
    ):
        # delete existing and create new input directory and add nifti file
        t2_file = create_input_file(
            "t2w_anatomical", "T2_run-01.nii.gz", touch_file=True
        )

        add_inputs = get_additional_inputs(log)

    # Assert T2w file is found
    assert t2_file in add_inputs

    # Assert '-T2' in add_inputs (count = 1)
    assert add_inputs.count("-T2") == 1
