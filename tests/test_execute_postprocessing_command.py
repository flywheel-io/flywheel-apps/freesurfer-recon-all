"""Unit test for execute_postprocessing_command."""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import execute_postprocesing_command

log = logging.getLogger(__name__)

mock_metadata = {}
mock_dry_run = True
mock_environ = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}
mock_config = {
    "gear-convert_volumes": False,
    "gear-convert_stats": False,
    "gear-convert_surfaces": False,
    "gear-register_surfaces": False,
    "gear-hippocampal_subfields": True,
    "gear-brainstem_structures": True,
    "gear-thalamic_nuclei": False,
    "gear-hypothalamic_subunits": False,
    "gear_gtmseg": False,
    "openmp": 4,
}


@patch("fw_gear_freesurfer_recon_all.main.do_gear_hippocampal_subfields")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_brainstem_structures")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_thalamic_nuclei")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_hypothalamic_subunits")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_register_surfaces")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_surfaces")
@patch("fw_gear_freesurfer_recon_all.main.do_gtmseg")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_volumes")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_stats")
def test_execute_postprocessing_command(
    mock_do_gear_convert_stats,
    mock_do_gear_convert_volumes,
    mock_do_gtmseg,
    mock_do_gear_convert_surfaces,
    mock_do_gear_register_surfaces,
    mock_do_gear_hypothalamic_subunits,
    mock_do_gear_thalamic_nuclei,
    mock_do_gear_brainstem_structures,
    mock_do_gear_hippocampal_subfields,
    mock_subject_dir,
):
    """Test execute_recon_all_command for preset config options."""

    errors, return_code, metadata = execute_postprocesing_command(
        mock_config,
        mock_environ,
        mock_dry_run,
        mock_subject_dir,
        mock_metadata,
        log,
    )

    assert mock_do_gear_hippocampal_subfields.call_count == 1
    assert mock_do_gear_brainstem_structures.call_count == 1
    assert mock_do_gear_convert_stats.call_count == 0
    assert mock_do_gear_convert_volumes.call_count == 0
    assert mock_do_gtmseg.call_count == 0
    assert mock_do_gear_convert_surfaces.call_count == 0
    assert mock_do_gear_register_surfaces.call_count == 0
    assert mock_do_gear_hypothalamic_subunits.call_count == 0
    assert mock_do_gear_thalamic_nuclei.call_count == 0

    assert not errors
    assert return_code == 0


@patch("fw_gear_freesurfer_recon_all.main.do_gear_hippocampal_subfields")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_brainstem_structures")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_thalamic_nuclei")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_hypothalamic_subunits")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_register_surfaces")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_surfaces")
@patch("fw_gear_freesurfer_recon_all.main.do_gtmseg")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_volumes")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_stats")
def test_execute_postprocessing_command_runtime_error(
    mock_do_gear_convert_stats,
    mock_do_gear_convert_volumes,
    mock_do_gtmseg,
    mock_do_gear_convert_surfaces,
    mock_do_gear_register_surfaces,
    mock_do_gear_hypothalamic_subunits,
    mock_do_gear_thalamic_nuclei,
    mock_do_gear_brainstem_structures,
    mock_do_gear_hippocampal_subfields,
    mock_subject_dir,
):
    """Test the call execute_postprocessing_all_command for a RuntimeError."""

    mock_config["gear-brainstem_structures"] = False

    # Check for two RuntimeErrors, as call should be run twice.
    expected_errors = [RuntimeError("Unable to execute command.")] * 2
    expected_return_code = 1
    with patch(
        "fw_gear_freesurfer_recon_all.main.do_gear_hippocampal_subfields",
        side_effect=RuntimeError("Unable to execute command."),
    ):
        errors, return_code, metadata = execute_postprocesing_command(
            mock_config,
            mock_environ,
            mock_dry_run,
            mock_subject_dir,
            mock_metadata,
            log,
        )

    # assertions:
    assert str(errors) == str(expected_errors)  # convert to strings to compare
    assert return_code == expected_return_code


@patch("fw_gear_freesurfer_recon_all.main.do_gear_hippocampal_subfields")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_brainstem_structures")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_thalamic_nuclei")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_hypothalamic_subunits")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_register_surfaces")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_surfaces")
@patch("fw_gear_freesurfer_recon_all.main.do_gtmseg")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_volumes")
@patch("fw_gear_freesurfer_recon_all.main.do_gear_convert_stats")
def test_execute_postprocessing_command_mri_dir_called(
    mock_do_gear_convert_stats,
    mock_do_gear_convert_volumes,
    mock_do_gtmseg,
    mock_do_gear_convert_surfaces,
    mock_do_gear_register_surfaces,
    mock_do_gear_hypothalamic_subunits,
    mock_do_gear_thalamic_nuclei,
    mock_do_gear_brainstem_structures,
    mock_do_gear_hippocampal_subfields,
    mock_subject_dir,
):
    """Test execute_recon_all_command uses mri_dir."""

    mock_config["gear-brainstem_structures"] = False
    mri_dir = f"{mock_subject_dir}/mri"

    execute_postprocesing_command(
        mock_config,
        mock_environ,
        mock_dry_run,
        mock_subject_dir,
        mock_metadata,
        log,
    )

    # Check that do_gear_hippocampal_subfields was called with mri_dir.
    # This approach seems to work, as test will fail if mri_dir
    # definition is commented out or removed
    # from test_execute_postprocessing_command.
    assert mock_do_gear_hippocampal_subfields.call_once_with(mri_dir)
