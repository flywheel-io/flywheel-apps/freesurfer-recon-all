"""Unit test for do_gear_hippocampal_subfields.py"""

import logging

from fw_gear_freesurfer_recon_all.main import do_gear_hippocampal_subfields
from tests.conftest import ASSETS_ROOT

log = logging.getLogger(__name__)

MOCK_SUBJECT_ID = "sub-01"
MOCK_DRY_RUN = False
MOCK_ENVIRON = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}
MOCK_TEXT_FILES = [
    "lh.hippoSfVolumes-T1.v21.txt",
    "rh.hippoSfVolumes-T1.v21.txt",
    "lh.amygNucVolumes-T1.v21.txt",
    "rh.amygNucVolumes-T1.v21.txt",
]


def test_do_gear_hippocampal_subfields_tablefile_exists(
    mocker,
    mock_subject_dir,
):
    """Test do_gear_hippocampal_subfields for output csv files already in output folder."""

    # Mock the external functions
    mock_exec_command = mocker.patch("fw_gear_freesurfer_recon_all.main.exec_command")
    mock_text_file_to_csv_file = mocker.patch(
        "fw_gear_freesurfer_recon_all.main.text_file_to_csv_file"
    )

    # Setup for the test
    mock_mri_dir = mock_subject_dir / "mri"

    # Using mocker to patch the OUTPUT_DIR
    mocker.patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", ASSETS_ROOT)

    # Call the function under test
    do_gear_hippocampal_subfields(
        MOCK_SUBJECT_ID,
        mock_mri_dir,
        MOCK_DRY_RUN,
        MOCK_ENVIRON,
        log,
    )

    # Expect one call for segmentHA_T1.sh.
    assert mock_exec_command.call_count == 1

    # Build list of arguments for expected call to exec_command
    expected_calls = [["segmentHA_T1.sh", str(MOCK_SUBJECT_ID)]]

    # Check that expected call of exec_command was made with correct arguments
    for c, cll in enumerate(expected_calls):
        mock_call = mock_exec_command.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll
        assert kwargs == {
            "environ": MOCK_ENVIRON,
            "dry_run": MOCK_DRY_RUN,
            "cont_output": True,
        }

    # Expect one call to text_file_to_csv_file for each
    # of four output txt files in MOCK_TEXT_FILES.
    assert mock_text_file_to_csv_file.call_count == len(MOCK_TEXT_FILES)

    # Build list of arguments for each expected call to text_file_to_csv_file
    expected_calls = []
    for tf in MOCK_TEXT_FILES:
        tablefile = f"{ASSETS_ROOT}/{MOCK_SUBJECT_ID}_{tf.replace('.txt', '.csv')}"
        expected_calls.append(
            [
                f"{mock_mri_dir}/{tf}",
                tablefile,
                True,
                MOCK_DRY_RUN,
            ]
        )

    # Check that expected call of text_file_to_csv_file was
    # made with correct arguments.
    for c, cll in enumerate(expected_calls):
        mock_call = mock_text_file_to_csv_file.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll[0]
        assert args[1] == cll[1]
        assert kwargs == {
            "pivot": True,
            "dry_run": MOCK_DRY_RUN,
        }


def test_do_gear_hippocampal_subfields_tablefile_not_exists(
    mocker,
    create_input_file,
    output_dir,
    mock_subject_dir,
    caplog,
):
    """Test do_gear_hippocampal_subfields for preset config options."""

    # Mock the external functions
    mock_exec_command = mocker.patch("fw_gear_freesurfer_recon_all.main.exec_command")
    mock_text_file_to_csv_file = mocker.patch(
        "fw_gear_freesurfer_recon_all.main.text_file_to_csv_file"
    )

    # Setup for the test
    mock_mri_dir = mock_subject_dir / "mri"

    # Using mocker to patch the OUTPUT_DIR
    mocker.patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", ASSETS_ROOT)

    # Create empty output directory for csv files to mock missing output files
    for tf in MOCK_TEXT_FILES:
        _ = create_input_file(
            anat_dir_name=output_dir,
            anat_file_name=f"{output_dir}/{MOCK_SUBJECT_ID}_{tf.replace('.txt', '.csv')}",
            touch_file=False,
        )

    do_gear_hippocampal_subfields(
        MOCK_SUBJECT_ID,
        mock_mri_dir,
        MOCK_DRY_RUN,
        MOCK_ENVIRON,
        log,
    )

    # Expect one call for segmentHA_T1.sh.
    assert mock_exec_command.call_count == 1

    # Expect four calls to text_file_to_csv_file for each
    # of four output txt files in MOCK_TEXT_FILES.
    assert mock_text_file_to_csv_file.call_count == len(MOCK_TEXT_FILES)

    # Check that expected log message was generated for missing output files
    for record in caplog.records:
        assert "is missing" in record.message
