"""Unit tests for generate_command."""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import generate_command

log = logging.getLogger(__name__)

subject_id = "That's Mr. Subject to you Pal!"


@patch(
    "fw_gear_freesurfer_recon_all.main.check_for_previous_run", return_value=subject_id
)
@patch("fw_gear_freesurfer_recon_all.main.get_input_file")
@patch("fw_gear_freesurfer_recon_all.main.get_additional_inputs")
def test_generate_command_works(
    mock_get_additional_inputs,
    mock_get_input_file,
    mock_check_for_previous_runs,
):
    """Test that generate_command works."""

    command_config = {
        "parallel": True,
        "reconall_options": "-all -qcache",
        "openmp": 11,
        "expert": "/flywheel/v0/input/expert/expert.opts",
    }

    command = generate_command(subject_id, command_config, log)

    assert mock_check_for_previous_runs.called_with(log)
    assert mock_get_input_file.called_with(log)
    assert mock_get_additional_inputs.called_with(log)

    assert command[0] == "time"
    assert command[1] == "recon-all"
    assert "-subjid" in command
    assert command[command.index("-subjid") + 1] == subject_id
    assert "-expert" in command
    assert command[command.index("-expert") + 1] == command_config["expert"]
