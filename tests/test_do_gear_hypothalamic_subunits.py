"""Unit test for do_gear_hypothalamic_subunits"""

import logging
from pathlib import Path

from fw_gear_freesurfer_recon_all.main import do_gear_hypothalamic_subunits

log = logging.getLogger(__name__)


def test_do_gear_hypothalamic_subunits_tablefile_exists(
    mocker,
    mock_subject_dir,
    output_dir,
):
    """Test do_gear_hypothalamic_subunits."""

    # Setup for the test
    mock_subject_id = "sub-01"
    mock_dry_run = False
    mock_environ = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}
    mock_config = {"openmp": 4}

    mock_mri_dir = mock_subject_dir / "mri"
    mock_in_file = Path(mock_mri_dir / "hypothalamic_subunits_volumes.v1.csv")
    mock_out_file = Path(
        output_dir / f"{mock_subject_id}_hypothalamic_subunits_volumes.v1.csv"
    )

    # Mock the external functions
    mock_exec_command = mocker.patch("fw_gear_freesurfer_recon_all.main.exec_command")
    mock_shutil_copy = mocker.patch("fw_gear_freesurfer_recon_all.main.shutil.copy")

    # Using mocker to patch the OUTPUT_DIR
    mocker.patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", output_dir)

    do_gear_hypothalamic_subunits(
        mock_subject_id,
        mock_mri_dir,
        mock_dry_run,
        mock_environ,
        mock_config["openmp"],
        log,
    )

    # Expect one call for mri_segment_hypothalamic_subunits
    assert mock_exec_command.call_count == 1

    # Build list of arguments for expected call to exec_command
    expected_call = [
        "mri_segment_hypothalamic_subunits",
        "--s",
        str(mock_subject_id),
        "--threads",
        str(mock_config["openmp"]),
    ]

    # Check that exec_command was called with the expected arguments
    mock_exec_command.assert_called_with(
        expected_call, environ=mock_environ, dry_run=mock_dry_run, cont_output=True
    )

    # Check that shutil was called with input and output
    mock_shutil_copy.assert_called_with(mock_in_file, mock_out_file)
