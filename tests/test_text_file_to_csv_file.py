"""Unit test for text_file_to_csv_file."""

from pathlib import Path
from unittest.mock import patch

import pandas as pd

from fw_gear_freesurfer_recon_all.main import text_file_to_csv_file
from tests.conftest import ASSETS_ROOT


def test_text_file_to_csv_file_no_pivot(output_dir):
    """Test text_file_to_csv_file for no pivot.

    Args:
    output_dir (path): temporary output directory from conftest.py

    """

    mock_dry_run = False

    # Define the input and output files
    input_no_pivot = ASSETS_ROOT / "template_text_file_no_pivot.txt"
    output_no_pivot = output_dir / "test_text_file_no_pivot.csv"

    # Create the output directory and file
    if not Path(output_no_pivot).exists():
        output_dir.mkdir(parents=True)
        output_no_pivot.touch(exist_ok=True)

    with patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", output_dir):
        text_file_to_csv_file(
            input_no_pivot, output_no_pivot, pivot=False, dry_run=mock_dry_run
        )

    # Load in the template and test csv files
    template_df = pd.read_csv(ASSETS_ROOT / "template_text_file_no_pivot.csv")
    test_df = pd.read_csv(output_no_pivot)

    # Check if the test csv file is equal to the template csv file
    are_equal = template_df.equals(test_df)
    assert are_equal is True


def test_text_file_to_csv_file_pivot(output_dir):
    """Test text_file_to_csv_file with pivot.

    Args:
    output_dir (path): temporary output directory from conftest.py

    """

    mock_dry_run = False

    # Define the input and output files
    input_pivot = ASSETS_ROOT / "template_text_file_pivot.txt"
    output_pivot = output_dir / "test_text_file_pivot.csv"

    # Create the output directory and file
    if not Path(output_pivot).exists():
        output_dir.mkdir(parents=True)
        output_pivot.touch(exist_ok=True)

    with patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", output_dir):
        text_file_to_csv_file(
            input_pivot, output_pivot, pivot=True, dry_run=mock_dry_run
        )

    # Load in the template and test csv files
    template_df = pd.read_csv(ASSETS_ROOT / "template_text_file_pivot.csv")
    test_df = pd.read_csv(output_pivot)

    # Check if the test csv file is equal to the template csv file
    are_equal = template_df.equals(test_df)
    assert are_equal is True


def test_text_file_to_csv_file_dry_run(output_dir, caplog):
    """Test text_file_to_csv_file with dry run.

    Args:
    output_dir (path): temporary output directory from conftest.py
    caplog (feature): pytest log capture feature

    """

    mock_dry_run = True

    # Define the input and output files
    input_pivot = ASSETS_ROOT / "template_text_file_pivot.txt"
    output_pivot = output_dir / "test_text_file_pivot.csv"

    with patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", output_dir):
        text_file_to_csv_file(
            input_pivot, output_pivot, pivot=True, dry_run=mock_dry_run
        )

    # Check that dry run was logged
    for record in caplog.records:
        assert "Dry run: Would have converted" in record.message
