"""Shared fixtures for testing."""

import shutil
import tempfile
from pathlib import Path

import pytest

ASSETS_ROOT = Path(__file__).parent / "assets"


@pytest.fixture
def flywheel_base():
    """Create a temporary flywheel directory for testing."""
    with tempfile.TemporaryDirectory() as tmp_dir_name:
        FLYWHEEL_BASE = Path(tmp_dir_name)
    return FLYWHEEL_BASE


@pytest.fixture
def input_dir(flywheel_base):
    """Create a temporary input directory for testing."""
    INPUT_DIR = Path(flywheel_base / "input")
    return INPUT_DIR


@pytest.fixture
def output_dir(flywheel_base):
    """Create a temporary output directory for testing."""
    OUTPUT_DIR = Path(flywheel_base / "output")
    return OUTPUT_DIR


@pytest.fixture
def mock_subject_dir(input_dir):
    """Create a temporary subject directory and return its path.

    This overrides the hard-coded container paths in main.py.

    Args:
    tmp_path: fixture used to create a temporary directory

    Returns:
    subject_dir: path to the temporary subject directory
    """

    subject_dir = input_dir / "sub-01"
    if not subject_dir.exists():
        subject_dir.mkdir(parents=True)

    return subject_dir


@pytest.fixture
def create_input_file(input_dir):
    """Create input file in input directory.

    Args:
    anat_dir_name (str): name of the directory to create
    anat_file_name (str): name of the file to create
    touch_file (bool): whether to create the file

    Returns:
    anat_file_path (str): path to the created file
    """

    def _create_input_file(anat_dir_name, anat_file_name, touch_file=True):
        anat_dir = input_dir / anat_dir_name
        if anat_dir.exists():
            shutil.rmtree(anat_dir)
        anat_dir.mkdir(parents=True)
        if touch_file:
            (anat_dir / anat_file_name).touch(exist_ok=True)

        return str(anat_dir / anat_file_name)

    return _create_input_file
