"""Unit tests for check_for_previous_run."""

import logging
import tempfile
from pathlib import Path
from unittest.mock import patch

import pytest

from fw_gear_freesurfer_recon_all.main import check_for_previous_run

log = logging.getLogger(__name__)

with tempfile.TemporaryDirectory() as tmp_dir_name:
    # Create temporary directories for testing
    FLYWHEEL_BASE = Path(tmp_dir_name)
    INPUT_DIR = Path(FLYWHEEL_BASE / "input")
    SUBJECTS_DIR = Path(FLYWHEEL_BASE / "subjects")

# create required input directory and zip file
anatomical = INPUT_DIR / "anatomical"
if not anatomical.exists():
    anatomical.mkdir(parents=True)
(anatomical / "freesurfer-recon-all_sub01.zip").touch(exist_ok=True)


@patch("fw_gear_freesurfer_recon_all.main.zipfile")
@patch("fw_gear_freesurfer_recon_all.main.unzip_archive")
@patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", FLYWHEEL_BASE)
@patch("fw_gear_freesurfer_recon_all.main.INPUT_DIR", INPUT_DIR)
@patch("fw_gear_freesurfer_recon_all.main.SUBJECTS_DIR", SUBJECTS_DIR)
def test_check_for_previous_run_successful(mock_unzip_archive, mock_zipfile):
    """Test check_for_previous_run finds zip file."""

    # create required subjects directory
    subject_label = "sub001"
    subject_dir = SUBJECTS_DIR / subject_label
    if not subject_dir.exists():
        subject_dir.mkdir(parents=True)

    # mock zip file to return True and return a namelist()
    with patch("fw_gear_freesurfer_recon_all.main.zipfile") as mock_zipfile1:
        mock_zipfile1.is_zipfile.return_value = True
        mock_zipfile1.namelist.return_value = [f"{subject_label}/foo"]

        with patch("fw_gear_freesurfer_recon_all.main.zipfile.ZipFile") as mock_ZipFile:
            mock_ZipFile.return_value = mock_zipfile1

            new_subject_id = check_for_previous_run(log)

    assert new_subject_id == subject_label


@patch("fw_gear_freesurfer_recon_all.main.zipfile")
@patch("fw_gear_freesurfer_recon_all.main.unzip_archive")
@patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", FLYWHEEL_BASE)
@patch("fw_gear_freesurfer_recon_all.main.INPUT_DIR", INPUT_DIR)
@patch("fw_gear_freesurfer_recon_all.main.SUBJECTS_DIR", SUBJECTS_DIR)
def test_check_for_previous_run_empty_new_subject_id(mock_unzip_archive, mock_zipfile):
    """Test check_for_previous_run sets empty new_subject_id correctly."""

    # mock zip file to return True and return an empty namelist()
    with patch("fw_gear_freesurfer_recon_all.main.zipfile") as mock_zipfile2:
        mock_zipfile2.is_zipfile.return_value = True
        mock_zipfile2.namelist.return_value = ["", ""]

        with patch("fw_gear_freesurfer_recon_all.main.zipfile.ZipFile") as mock_ZipFile:
            mock_ZipFile.return_value = mock_zipfile2

            new_subject_id = check_for_previous_run(log)

    assert new_subject_id == ""


@patch("fw_gear_freesurfer_recon_all.main.zipfile")
@patch("fw_gear_freesurfer_recon_all.main.unzip_archive")
@patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", FLYWHEEL_BASE)
@patch("fw_gear_freesurfer_recon_all.main.INPUT_DIR", INPUT_DIR)
@patch("fw_gear_freesurfer_recon_all.main.SUBJECTS_DIR", SUBJECTS_DIR)
def test_check_for_previous_run_no_subject_dir(
    mock_unzip_archive, mock_zipfile, caplog
):
    """Test check_for_previous_run exits with missing subject_dir."""

    # mock zip file to return True and return a namelist()
    with patch("fw_gear_freesurfer_recon_all.main.zipfile") as mock_zipfile3:
        mock_zipfile3.is_zipfile.return_value = True
        mock_zipfile3.namelist.return_value = [
            str("sub01/T1.nii.gz"),
            str("sub01/T2.nii.gz"),
        ]

        with patch("fw_gear_freesurfer_recon_all.main.zipfile.ZipFile") as mock_ZipFile:
            mock_ZipFile.return_value = mock_zipfile3

            with pytest.raises(SystemExit) as exc:
                _ = check_for_previous_run(log)

    assert exc.type is SystemExit
    assert exc.value.code == 1
    for record in caplog.records:
        assert (
            "No SUBJECT DIR could be found! Cannot continue. Exiting" in record.message
        )
        assert record.levelname == "CRITICAL"
