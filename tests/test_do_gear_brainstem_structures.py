"""Unit test for do_gear_brainstem_structures.py"""

import logging

from fw_gear_freesurfer_recon_all.main import do_gear_brainstem_structures
from tests.conftest import ASSETS_ROOT

log = logging.getLogger(__name__)

MOCK_SUBJECT_ID = "sub-01"
MOCK_DRY_RUN = False
MOCK_ENVIRON = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}


def test_do_gear_brainstem_structures_tablefile_exists(
    mocker,
    mock_subject_dir,
):
    """Test do_gear_brainstem_structures for output csv file already in output folder."""

    # Mock the external functions
    mock_exec_command = mocker.patch("fw_gear_freesurfer_recon_all.main.exec_command")
    mock_text_file_to_csv_file = mocker.patch(
        "fw_gear_freesurfer_recon_all.main.text_file_to_csv_file"
    )

    # Setup for the test
    mock_mri_dir = mock_subject_dir / "mri"

    # Using mocker to patch the OUTPUT_DIR
    mocker.patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", ASSETS_ROOT)

    do_gear_brainstem_structures(
        MOCK_SUBJECT_ID,
        mock_mri_dir,
        MOCK_DRY_RUN,
        MOCK_ENVIRON,
        log,
    )

    # Expect two calls for segmentBS.sh and quantifyBrainstemStructures.sh.
    assert mock_exec_command.call_count == 2

    # Build list of arguments for each expected call to exec_command
    # Hard code first two calls to shell script commands
    expected_calls = [["segmentBS.sh", str(MOCK_SUBJECT_ID)]]
    expected_calls.append(
        [
            "quantifyBrainstemStructures.sh",
            f"{mock_mri_dir}/brainstemSsVolumes.v2.txt",
        ]
    )

    # Check that each expected call of exec_command was made with correct list of arguments
    for c, cll in enumerate(expected_calls):
        mock_call = mock_exec_command.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll
        assert kwargs == {
            "environ": MOCK_ENVIRON,
            "dry_run": MOCK_DRY_RUN,
            "cont_output": True,
        }

    # Expect one call to text_file_to_csv_file
    assert mock_text_file_to_csv_file.call_count == 1

    # Build text_file_to_csv_file command arguments
    expected_calls = [
        [
            f"{mock_mri_dir}/brainstemSsVolumes.v2.txt",
            f"{ASSETS_ROOT}/{MOCK_SUBJECT_ID}_brainstemSsVolumes.v2.csv",
            False,
            MOCK_DRY_RUN,
        ]
    ]

    # Check that each expected call of text_file_to_csv_file
    # was made with correct list of arguments
    for c, cll in enumerate(expected_calls):
        mock_call = mock_text_file_to_csv_file.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll[0]  # Check first argument is input
        assert args[1] == cll[1]  # Check second argument is output
        assert kwargs == {
            "pivot": False,
            "dry_run": MOCK_DRY_RUN,
        }


def test_do_gear_brainstem_structures_tablefile_not_exists(
    mocker,
    create_input_file,
    output_dir,
    mock_subject_dir,
    caplog,
):
    """Test do_gear_brainstem_structures for output csv file not in output folder."""

    # Mock the external functions
    mock_exec_command = mocker.patch("fw_gear_freesurfer_recon_all.main.exec_command")
    mock_text_file_to_csv_file = mocker.patch(
        "fw_gear_freesurfer_recon_all.main.text_file_to_csv_file"
    )

    # Setup for the test
    mock_mri_dir = mock_subject_dir / "mri"

    # Using mocker to patch the OUTPUT_DIR
    mocker.patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", ASSETS_ROOT)

    # Create an empty output directory
    tablefile = f"{output_dir}/{MOCK_SUBJECT_ID}_brainstemSsVolumes.v2.csv"
    _ = create_input_file(
        anat_dir_name=output_dir,
        anat_file_name=f"{output_dir}/{MOCK_SUBJECT_ID}_{tablefile.replace('.txt', '.csv')}",
        touch_file=False,
    )

    do_gear_brainstem_structures(
        MOCK_SUBJECT_ID,
        mock_mri_dir,
        MOCK_DRY_RUN,
        MOCK_ENVIRON,
        log,
    )

    # Expect two calls for segmentBS.sh and quantifyBrainstemStructures.sh.
    assert mock_exec_command.call_count == 2

    # Expect one call to text_file_to_csv_file.
    assert mock_text_file_to_csv_file.call_count == 1

    # Check that expected log message was generated for missing output file
    for record in caplog.records:
        assert "is missing" in record.message
