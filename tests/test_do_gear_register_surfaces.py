"""Unit test for do_gear_register_surfaces"""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import do_gear_register_surfaces

log = logging.getLogger(__name__)

MOCK_SUBJECT_ID = "sub-01"
MOCK_ENVIRON = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_do_gear_register_surfaces_dry_run_false(mock_exec_command):
    """Test do_gear_register_surfaces for dry_run=False."""

    mock_dry_run = False

    do_gear_register_surfaces(
        MOCK_SUBJECT_ID,
        mock_dry_run,
        MOCK_ENVIRON,
        log,
    )

    # Expect three calls to exec_command: "xhemireg", "surfreg", "surfreg"
    assert mock_exec_command.call_count == 3

    # Build list of arguments for expected call to exec_command
    # checking that the subject id is passed to each command
    expected_calls = [["xhemireg", "--s", MOCK_SUBJECT_ID]]
    expected_calls.append(
        ["surfreg", "--s", MOCK_SUBJECT_ID, "--t", "fsaverage_sym", "--lh"]
    )
    expected_calls.append(
        [
            "surfreg",
            "--s",
            MOCK_SUBJECT_ID,
            "--t",
            "fsaverage_sym",
            "--lh",
            "--xhemi",
        ]
    )

    # Check that each expected call of exec_command was made
    # with correct args and kwargs
    for c, cll in enumerate(expected_calls):
        mock_call = mock_exec_command.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll
        assert kwargs == {
            "environ": MOCK_ENVIRON,
            "dry_run": mock_dry_run,
            "cont_output": True,
        }


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_do_gear_register_surfaces_dry_run_true(mock_exec_command):
    """Test do_gear_register_surfaces for dry_run=True.

    Behavior should not change from dry_run=False.

    """

    mock_dry_run = True

    do_gear_register_surfaces(
        MOCK_SUBJECT_ID,
        mock_dry_run,
        MOCK_ENVIRON,
        log,
    )

    # Expect three calls to exec_command: "xhemireg", "surfreg", "surfreg"
    assert mock_exec_command.call_count == 3

    # Build list of arguments for expected call to exec_command
    # checking that the subject id is passed to each command
    expected_calls = [["xhemireg", "--s", MOCK_SUBJECT_ID]]
    expected_calls.append(
        ["surfreg", "--s", MOCK_SUBJECT_ID, "--t", "fsaverage_sym", "--lh"]
    )
    expected_calls.append(
        [
            "surfreg",
            "--s",
            MOCK_SUBJECT_ID,
            "--t",
            "fsaverage_sym",
            "--lh",
            "--xhemi",
        ]
    )

    # Check that each expected call of exec_command was made
    # with correct args and kwargs
    for c, cll in enumerate(expected_calls):
        mock_call = mock_exec_command.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll
        assert kwargs == {
            "environ": MOCK_ENVIRON,
            "dry_run": mock_dry_run,
            "cont_output": True,
        }
