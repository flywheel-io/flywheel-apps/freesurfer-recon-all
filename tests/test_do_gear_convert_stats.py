"""Unit test for do_gear_convert_stats.py"""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import do_gear_convert_stats
from tests.conftest import ASSETS_ROOT

log = logging.getLogger(__name__)


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_do_gear_convert_stats_tablefile_exists(mock_exec_command):
    """Test do_gear_convert_stats for output csv files already in output folder."""

    mock_subject_id = "sub-01"
    mock_dry_run = False
    mock_environ = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}

    # Create list of output csv files to be created by do_gear_convert_stats
    # List should be in same order as calls to exec_command
    mock_table_files = [
        "aseg_stats_vol_mm3.csv",
        "lh_aparc.a2009s_stats_area_mm2.csv",
        "lh_aparc_stats_area_mm2.csv",
        "lh_aparc.DKTatlas_stats_area_mm2.csv",
        "lh_aparc.pial_stats_area_mm2.csv",
        "rh_aparc.a2009s_stats_area_mm2.csv",
        "rh_aparc_stats_area_mm2.csv",
        "rh_aparc.DKTatlas_stats_area_mm2.csv",
        "rh_aparc.pial_stats_area_mm2.csv",
    ]

    with patch("fw_gear_freesurfer_recon_all.main.OUTPUT_DIR", ASSETS_ROOT):
        do_gear_convert_stats(
            mock_subject_id,
            mock_dry_run,
            mock_environ,
            log,
        )

    # Expect one call for each file in mock_table_files.
    assert mock_exec_command.call_count == len(mock_table_files)

    # Build list of arguments for each expected call to exec_command
    expected_calls = []
    # Add cmd for aseg_stats_vol_mm3.csv file
    tablefile = f"{ASSETS_ROOT}/{mock_subject_id}_aseg_stats_vol_mm3.csv"
    expected_calls.append(
        [
            "asegstats2table",
            "-s",
            mock_subject_id,
            "--delimiter",
            "comma",
            f"--tablefile={tablefile}",
        ]
    )
    # Add cmd for aseg_stats_area_mm2.csv files
    for hh in ["lh", "rh"]:
        for pp in ["aparc.a2009s", "aparc", "aparc.DKTatlas", "aparc.pial"]:
            tablefile = f"{ASSETS_ROOT}/{mock_subject_id}_{hh}_{pp}_stats_area_mm2.csv"
            expected_calls.append(
                [
                    "aparcstats2table",
                    "-s",
                    mock_subject_id,
                    f"--hemi={hh}",
                    "--delimiter=comma",
                    f"--parc={pp}",
                    f"--tablefile={tablefile}",
                ]
            )

    # Check that each expected call of exec_command was made
    # with correct args and kwargs
    for c, cll in enumerate(expected_calls):
        mock_call = mock_exec_command.call_args_list[c]
        args, kwargs = mock_call
        assert args[0] == cll
        assert kwargs == {
            "environ": mock_environ,
            "dry_run": mock_dry_run,
            "cont_output": True,
        }
