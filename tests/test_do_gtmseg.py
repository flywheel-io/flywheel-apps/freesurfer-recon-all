"""Unit test for do_gtmseg.py"""

import logging
from unittest.mock import patch

from fw_gear_freesurfer_recon_all.main import do_gtmseg

log = logging.getLogger(__name__)


@patch("fw_gear_freesurfer_recon_all.main.exec_command")
def test_do_gtmseg(mock_exec_command):
    """Test do_gtmseg."""

    mock_subject_id = "sub-01"
    mock_dry_run = False
    mock_environ = {"FREESURFER_HOME": ["/usr/local/freesurfer"]}

    do_gtmseg(
        mock_subject_id,
        mock_dry_run,
        mock_environ,
        log,
    )

    # Expect one call for "gtmseg" command
    assert mock_exec_command.call_count == 1

    # Build list of arguments for expected call to exec_command
    expected_call = ["gtmseg", "--s", mock_subject_id]

    # Check that exec_command was called with the expected arguments
    mock_exec_command.assert_called_with(
        expected_call, environ=mock_environ, dry_run=mock_dry_run, cont_output=True
    )
