"""Unit test for make_file_name_safe."""

import pytest

from utils.fly.make_file_name_safe import make_file_name_safe

data = [
    ("a_filename_with_safe_characters", "_", "a_filename_with_safe_characters"),
    ("a$filename%with&unsafe*characters", "_", "a_filename_with_unsafe_characters"),
    ("a_filename_with_safe_characters", "*", "a_filename_with_safe_characters"),
    ("a$filename%with&unsafe*characters", "*", "afilenamewithunsafecharacters"),
    ("", "x", ""),
]


@pytest.mark.parametrize("input_filename, replacement_str, expected", data)
def test_make_file_name_safe_works(input_filename, replacement_str, expected):
    """Test that make_file_name_safe works as expected.

    Tests that make_file_name_safe returns a string with
    only safe characters, and that it replaces or removes unsafe
    characters. Tests with both safe and unsafe filenames
    and both safe and unsafe replacement strings.
    """
    safe_filename = make_file_name_safe(input_filename, replacement_str)
    assert safe_filename == expected


data = [
    (None, "_", TypeError),
]


@pytest.mark.parametrize("input_filename, replacement_str, expected", data)
def test_make_file_name_safe_fails(input_filename, replacement_str, expected):
    """Test failure cases for make_file_name_safe."""
    if isinstance(expected, Exception) or issubclass(expected, Exception):
        with pytest.raises(expected):
            make_file_name_safe(input_filename, replacement_str)
    else:
        safe_filename = make_file_name_safe(input_filename, replacement_str)
        assert safe_filename == expected
