"""Unit tests for get_input_file."""

import logging
import os
from pathlib import Path
from unittest.mock import MagicMock, patch

import pytest

from fw_gear_freesurfer_recon_all.main import get_input_file

log = logging.getLogger(__name__)


def mock_unzip_archive(self, dicom_dir):
    """Mock unzip_archive to add files to dicom_dir."""
    dicom_dir = Path(dicom_dir)
    (dicom_dir / "dicom_0001.dcm").touch(exist_ok=True)

    mock_function = MagicMock()
    return mock_function


mock_unzip_archive = MagicMock(side_effect=mock_unzip_archive)


@patch("fw_gear_freesurfer_recon_all.main.despace")
def test_get_input_file_one_nifti(
    mock_despace, flywheel_base, input_dir, create_input_file
):
    """Test get_input_file finds one nifti file."""

    with patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", flywheel_base), patch(
        "fw_gear_freesurfer_recon_all.main.INPUT_DIR", input_dir
    ):
        anat_file = create_input_file("anatomical", "T1.nii.gz", touch_file=True)

        anatomical = get_input_file(log)

    assert anatomical == anat_file


@patch("fw_gear_freesurfer_recon_all.main.despace")
def test_get_input_file_multiple_niftis(
    mock_despace, flywheel_base, input_dir, create_input_file, caplog
):
    """Test get_input_file finds multiple nifti files."""

    with patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", flywheel_base), patch(
        "fw_gear_freesurfer_recon_all.main.INPUT_DIR", input_dir
    ):
        anat_file_1, anat_file_2 = (
            create_input_file(
                anat_dir_name="anatomical",
                anat_file_name=f"T1_run-0{i}.nii.gz",
                touch_file=True,
            )
            for i in [1, 2]
        )

        # Get list of nifti files and select first file
        # Use rglob to match main.py methodology
        anat = Path(anat_file_1).parent
        anat_list = list(anat.rglob("*.nii*"))
        anat_file = str(anat_list[0])

        anatomical = get_input_file(log)

    assert anatomical == anat_file
    for record in caplog.records:
        assert "What?  Found NIfTI files!" in record.message
        assert record.levelname == "WARNING"


@patch("fw_gear_freesurfer_recon_all.main.despace")
@patch("fw_gear_freesurfer_recon_all.main.unzip_archive", new=mock_unzip_archive)
def test_get_input_file_zip_file(
    mock_despace, flywheel_base, input_dir, create_input_file
):
    """Test get_input_file finds zip file."""

    with patch(
        "fw_gear_freesurfer_recon_all.main.unzip_archive",
        side_effect=mock_unzip_archive,
    ), patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", flywheel_base), patch(
        "fw_gear_freesurfer_recon_all.main.INPUT_DIR", input_dir
    ):
        zip_file = create_input_file(
            "anatomical", "freesurfer-recon-all_sub001.zip", touch_file=True
        )

        anatomical = get_input_file(log)

    # Check that mock_unzip_archive is called and extracting the zip file
    assert mock_unzip_archive.called_once_with(
        zip_file, str(input_dir / "anatomical" / "dicom")
    )
    assert os.path.basename(anatomical) == "dicom_0001.dcm"


@patch("fw_gear_freesurfer_recon_all.main.despace")
def test_get_input_file_error(
    mock_despace, flywheel_base, input_dir, create_input_file, caplog
):
    """Test get_input_file exits with empty anat_dir."""

    with patch("fw_gear_freesurfer_recon_all.main.FLYWHEEL_BASE", flywheel_base), patch(
        "fw_gear_freesurfer_recon_all.main.INPUT_DIR", input_dir
    ):
        _ = create_input_file("anatomical", "T1.nii.gz", touch_file=False)

        with pytest.raises(SystemExit) as exc:
            _ = get_input_file(log)

    assert exc.type is SystemExit
    assert exc.value.code == 1
    for record in caplog.records:
        assert "Anatomical input could not be found" in record.message
        assert record.levelname == "CRITICAL"
