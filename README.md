# Freesurfer Recon-All

freesurfer-recon-all is a Flywheel Gear that runs the full Freesurfer
`recon-all` pipeline plus a number of Freesurfer-provided post-processing
scripts.

## Documentation

The documentation of the gear can be found
[here](https://flywheel-io.gitlab.io/scientific-solutions/gears/freesurfer-recon-all/index.html).

## Contributing

Please refer to the [CONTRIBUTING.md](CONTRIBUTING.md) file for information on how to
contribute to the gear.
