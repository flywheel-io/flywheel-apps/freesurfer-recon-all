"""Given string, return something safe to use as a file name."""

import logging
import re

log = logging.getLogger(__name__)


def make_file_name_safe(input_basename: str, replace_str: str = "") -> str:
    """Remove non-safe characters from a filename and return a filename with these characters replaced with replace_str.

    Args:
        input_basename (str): the input basename of the file to be replaced
        replace_str (str): the string with which to replace the unsafe characters

    Returns:
        output_basename (str): a safe transformation of the filename
    """
    unsafe_pattern = re.compile(r"[^A-Za-z0-9_\-.]+")
    # if the replacement is not a string or not safe, set replace_str to an empty string
    if not isinstance(replace_str, str) or unsafe_pattern.match(replace_str):
        log.warning("{} is not a safe string, removing instead".format(replace_str))
        replace_str = ""

    # Replace non-alphanumeric (or underscore) characters with replace_str
    safe_output_basename = re.sub(unsafe_pattern, replace_str, input_basename)

    if safe_output_basename.startswith("."):
        safe_output_basename = safe_output_basename[1:]

    if safe_output_basename != input_basename:
        log.debug('"%s" -> "%s"', input_basename, safe_output_basename)

    return safe_output_basename
